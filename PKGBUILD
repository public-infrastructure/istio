# Maintainer: Joel Cherney <joel@mutablemango.com>

pkgname=istio
pkgver=1.20.1
pkgrel=1
pkgdesc='Istio configuration command line utility for service operators to debug and diagnose their Istio mesh.'
arch=('x86_64')
url='https://github.com/istio/istio'
license=('Apache')
makedepends=('go')
conflicts=('istio')
source=("$pkgname-$pkgver.tar.gz::https://github.com/istio/istio/archive/$pkgver.tar.gz")
sha512sums=('3d0ea769f3cce4a6e0cc17feeb33955120b9a84ee7740fa4928f4aa486683bd53b0386ba936cb8e5857f1eb0fed4efc6287b5da07fbe5425ace592631e7151da')
b2sums=('0e0d1903a0fbfee3bab3d7f0ddf1fff36cd2b41e12ba0588ad012c2e8fad0d30e57dfa93c08b9668c9cc935580f742973f85c4d3433d516a9e15a8fb5efa67e1')

build() {
  cd $pkgname-$pkgver
  export VERSION=$pkgver
  export TAG=$pkgver
  export BUILD_WITH_CONTAINER=0
  make build
}

package() {
  install -Dm 755 "${srcdir}/$pkgname-$pkgver/out/linux_amd64/istioctl" "${pkgdir}/usr/bin/istioctl"

  # Populate bash and zsh completions
  install -dm 755 "${pkgdir}/usr/share/bash-completion/completions"
  install -dm 755 "${pkgdir}/usr/share/zsh/site-functions"
  "${pkgdir}/usr/bin/istioctl" collateral --bash
  mv istioctl.bash "${pkgdir}/usr/share/bash-completion/completions/istioctl"
  "${pkgdir}/usr/bin/istioctl" collateral --zsh
  mv _istioctl  "${pkgdir}/usr/share/zsh/site-functions/_istioctl"
}

